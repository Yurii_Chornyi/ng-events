import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  LogOrName:string = "Log In";
  ngOnInit(): void {
    if(localStorage.getItem('currentUser')){
      this.LogOrName = JSON.parse(localStorage.getItem('currentUser')).userName;
    }
  }
}
