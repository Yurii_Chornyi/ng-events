import { Component, OnInit } from '@angular/core';
import { UserModel } from '../models/UserModel';
import { OrganizationModel } from '../models/OrganizationModel';
import { AuthenticationService } from '../services/authentication.service';
import { AppService } from '../services/app.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from '../services/loading.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-organization',
  templateUrl: './register-organization.component.html',
  styleUrls: ['./register-organization.component.scss']
})
export class RegisterOrganizationComponent implements OnInit {

  constructor(private readonly authenticationService: AuthenticationService,
    private readonly appService: AppService, private readonly loadingService: LoadingService,
    private readonly router: Router,
    private readonly spinner: NgxSpinnerService) { }
  userModel: UserModel = { userName: "", email: "", phoneNumber: NaN, password: "" };
  orgModel: OrganizationModel = { name: "", description: "", picture: "" };

  ngOnInit() {
  }

  onSubmit() {
    this.spinner.show();
    this.loadingService.track(this.authenticationService.registerUser(this.userModel), (registeredUser) => {
      console.log(registeredUser);
      this.orgModel.administrator = registeredUser.id;
      localStorage.setItem('currentUser', JSON.stringify(registeredUser));
      this.loadingService.track(this.appService.registerOrg(this.orgModel), (registeredOrg) => {
        console.log(registeredOrg);
        this.spinner.hide();
      });
    });
  }
}
