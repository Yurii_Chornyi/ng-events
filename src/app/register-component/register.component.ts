import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { LoadingService } from '../services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  model = { name: "", email: "", phoneNumber: "", company: "", position: "", city: "", offlineParticipation: "", eventId: 0 };
  registered: boolean = false;
  constructor(private readonly appService: AppService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location,
    private readonly loadingService: LoadingService,
    private readonly spinner: NgxSpinnerService) { }


  ngOnInit() {
    this.activatedRoute.params.subscribe(x => this.model.eventId = x.eventId);
  }

  onSubmit() {
    console.log(this.model);
    this.spinner.show();
    this.loadingService.track(this.appService.register(this.model), response => {
      this.registered = response
      this.spinner.hide();
    });
  }

  goToEvent(){
    this.location.back();
  }

}
