import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app-component/app.component';
import { MatProgressSpinnerModule } from "@angular/material";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Router } from '@angular/router';
import { AppService } from './services/app.service';
import { LoadingService } from './services/loading.service';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { EventsComponent } from './events-component/events.component';
import { OrganizationsComponent } from './organizations-component/organizations.component';
import { RegisterComponent } from './register-component/register.component';
import { SharedModule } from './shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MatCardModule, MatIconModule, MatToolbarModule, MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { LoginComponent } from './login-component/login.component';
import { AuthenticationService } from './services/authentication.service';
import { AuthJwtInterceptor } from './services/auth.interceptor';
import { AuthGuard } from './services/auth.guard';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { RegisterOrganizationComponent } from './register-organization/register-organization.component';

@NgModule({
  providers: [AuthGuard, AppService, LoadingService, HttpClient, AuthenticationService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthJwtInterceptor,
    multi: true
  },],
  declarations: [AppComponent, EventsComponent, OrganizationsComponent, RegisterComponent, LoginComponent, AdminDashboardComponent, RegisterOrganizationComponent],
  imports: [SharedModule, FormsModule, MatCardModule, MatIconModule, MatToolbarModule,
    MatButtonModule, MatFormFieldModule, MatInputModule, HttpClientModule, BrowserModule,
    BrowserAnimationsModule, MatProgressSpinnerModule, NgxSpinnerModule,
    RouterModule.forRoot(
      [{
        path: '',
        component: OrganizationsComponent
      },
      {
        path: ':organizationUrl/events',
        component: EventsComponent
      },
      {
        path: ':organizationUrl/events/:eventId/register',
        component: RegisterComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'admin-dashboard',
        component: AdminDashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'register-organization',
        component: RegisterOrganizationComponent
      }
      ],
    ),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
