import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppService } from '../services/app.service';
import { LoadingService } from '../services/loading.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EventModel } from '../models/EventModel';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events: EventModel[];
  constructor(private readonly appService: AppService,
    private readonly loadingService: LoadingService,
    private readonly spinner: NgxSpinnerService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.spinner.show();
    let orgUrl = "";

    this.loadingService.track(this.activatedRoute.paramMap, (params) => orgUrl = params.get('organizationUrl'));
    this.loadingService.track(this.appService.getEventsByOrg(orgUrl),
      (events) => { this.events = events; this.spinner.hide() });
  }

  register(eventId: string){
    this.router.navigate([eventId, "register"], {relativeTo:this.activatedRoute});
  }

}
