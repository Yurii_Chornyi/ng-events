import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { UserModel } from '../models/UserModel';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  model: UserModel = Object.assign({}, this.model);

  constructor(
    private readonly activaterRoute: ActivatedRoute,
    private readonly loadingService: LoadingService,
    private readonly router: Router,
    private readonly spinner: NgxSpinnerService,
    private readonly authenticationService: AuthenticationService) { }

  ngOnInit() {
    if(localStorage.getItem('currentUser')){
      this.router.navigate(['../admin-dashboard']);
    }
  }

  // getAllUsers() {
  //   this.loadingService.track(this.authenticationService.getAllUsers(), (result) => console.log(result));
  // }

  login() {
    this.spinner.show();
    this.loadingService.track(this.authenticationService.login(this.model.email, this.model.password),
      user => {
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.spinner.hide();
          this.router.navigate(['../admin-dashboard']);
        }
      }
    )

  }
}
