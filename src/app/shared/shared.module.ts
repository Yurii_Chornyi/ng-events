import { NgModule } from '@angular/core';
import { ListTileComponent } from './components/list-tile/list-tile.component';

@NgModule({
  declarations: [ListTileComponent],
  imports: [],
  exports:[ListTileComponent]
})
export class SharedModule { }
