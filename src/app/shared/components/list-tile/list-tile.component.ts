import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OrganizationModel, OrganizationOrEvent } from '../../../models/OrganizationModel';
import { EventModel } from '../../../models/EventModel';

@Component({
  selector: 'list-tile',
  templateUrl: './list-tile.component.html',
  styleUrls: ['./list-tile.component.scss']
})
export class ListTileComponent implements OnInit {
  @Input() data: OrganizationOrEvent;
  @Input() actionName: string;
  @Output() actionClick = new EventEmitter<any>();
  constructor() {
  }

  // goNext(item: OrganizationOrEvent) {
  //   if ((item as OrganizationModel).organizationUrl) {
  //     this.navigateNext.emit((item as OrganizationModel).organizationUrl);
  //   }
  //   else {
  //     this.navigateNext.emit((item as EventModel).id.toString());
  //   }
  // }

  onActionClick(event){
    this.actionClick.emit(this.data.id);
  }

  ngOnInit() {
  }

}
