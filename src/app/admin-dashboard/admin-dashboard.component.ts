import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { LoadingService } from '../services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrganizationModel } from '../models/OrganizationModel';
import { ExtendedOrganizationModel } from '../models/ExtendedOrganizationModel';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  constructor(private readonly authenticationService: AuthenticationService, private readonly loadingService: LoadingService,
    private readonly spinner: NgxSpinnerService) { }


  isSysAdmin: boolean = false;
  pendingApproveOrgs: ExtendedOrganizationModel[];
  manageOrgs: OrganizationModel[];

  ngOnInit() {
    this.spinner.show();
    this.loadingService.track(this.authenticationService.isSysAdmin(), (isSysAdmin) => this.isSysAdmin = isSysAdmin);

  }

}
