import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { LoadingService } from '../services/loading.service';
import { OrganizationModel } from '../models/OrganizationModel';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss']
})
export class OrganizationsComponent implements OnInit {
  ngOnInit() {
    this.spinner.show();
    this.loadingService.track(this.appService.getOrganizations(), org => { this.organizations = org; this.spinner.hide() });
    //this.loadingService.loadingSubj.subscribe(x=>this.isLoading = x);
    //this.loadingService.finished.subscribe(() => this.spinner.hide());
    this.loadingService.failed.subscribe(() => this.isError = true);
  }

  isLoading: boolean = true;
  isError: boolean = false;
  organizations: OrganizationModel[];

  constructor(private readonly appService: AppService,
    private readonly loadingService: LoadingService,
    private readonly spinner: NgxSpinnerService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute) { }

  goToEvents(orgId: string) {
    let org = this.organizations.find(org => org.id == orgId);
    this.router.navigate([org.organizationUrl, 'events'], { relativeTo: this.activatedRoute });
  }
}
