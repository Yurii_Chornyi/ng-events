import { UserModel } from './UserModel';
import { OrganizationModel } from './OrganizationModel';

export interface ExtendedOrganizationModel extends OrganizationModel
{
    admin: UserModel
}