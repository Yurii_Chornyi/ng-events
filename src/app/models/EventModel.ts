import { OrganizationModel } from "./OrganizationModel";

export interface EventModel
{
    id: number;
    organization: OrganizationModel;
    name: string;
    description: string;
    picture: string;
}