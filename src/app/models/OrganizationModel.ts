import { EventModel } from "./EventModel";

export interface OrganizationModel
{
    id?: string;
    organizationUrl?: string;
    name: string;
    description: string;
    picture: string;
    administrator?: string;
    events?: EventModel[];
}

export type OrganizationOrEvent = OrganizationModel | EventModel