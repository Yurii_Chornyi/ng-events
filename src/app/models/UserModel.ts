export interface UserModel
{
    id?: string;
    userName: string;
    email: string;
    password: string;
    phoneNumber: number;
    token?: string;
}