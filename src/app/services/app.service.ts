import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrganizationModel } from '../models/OrganizationModel';
import { EventModel } from '../models/EventModel';

@Injectable()
export class AppService {
  constructor(private readonly httpClient: HttpClient) { }

  //private apiUrl: string = "http://web-events.azurewebsites.net/api/";
  private apiUrl: string = "https://localhost:44368/api/";

  getOrganizations(): Observable<OrganizationModel[]> {
    return this.httpClient.get<OrganizationModel[]>(this.apiUrl + 'organization/getAllOrganizations');
  }

  getEventsByOrg(orgUrl: string): Observable<EventModel[]> {
    return this.httpClient.get<EventModel[]>(this.apiUrl + `organization/${orgUrl}/getOrganizationEvents`);
  }

  register(model: any): Observable<boolean> {
    return this.httpClient.post<boolean>(this.apiUrl + `events/register`, model);
  }

  registerOrg(org: OrganizationModel): Observable<OrganizationModel> {
    return this.httpClient.post<OrganizationModel>(this.apiUrl + `organization/register`, org);
  }
}
