import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../models/UserModel';
import { Observable } from 'rxjs';

@Injectable()
export class AuthenticationService {
  constructor(private readonly httpClient: HttpClient) { }

  //private apiUrl: string = "http://web-events.azurewebsites.net/api/";
  private apiUrl: string = "https://localhost:44368/api/";

  login(email: string, password: string): Observable<UserModel> {
    return this.httpClient.post<UserModel>(this.apiUrl + 'users/login', { email: email, password: password });
  }

  // getAllUsers(): Observable<any> {
  //   return this.http.get<any>(this.apiUrl + 'users/get');
  // }

  registerUser(user: UserModel): Observable<UserModel> {
    return this.httpClient.post<UserModel>(this.apiUrl + 'users/register', user);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  isSysAdmin(): Observable<boolean>{
    var user = JSON.parse(localStorage.getItem('currentUser'));
    return this.httpClient.get<boolean>(this.apiUrl + 'users/issysadmin/'+ user.id);
  }
}
