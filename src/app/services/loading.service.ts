import {Injectable, EventEmitter, Optional} from '@angular/core'
import {Observable, Subscription} from 'rxjs'

export declare type SucceedFn<T> = (response?: T) => void;
export declare type FailedFn = (e?: Error) => void;
export declare type FinishedFn = () => boolean | void;

@Injectable()
export class LoadingService {

 started: EventEmitter<any> = new EventEmitter();
 finished: EventEmitter<any> = new EventEmitter();
 failed: EventEmitter<Error> = new EventEmitter<Error>();

 private currentlyRunning: number = 0;

 localizations = {
   errorNotification_TimeOut: 'Server Connection Timed Out',
   errorNotification_ClickToReload: 'Please click to reload.',
   errorNotification_Authorization: 'Authorization Error',
   errorNotification_NoPermissionsToRecord: 'You do not have permissions to access this record.',
   errorNotification_MissingRecord: 'Missing Record',
   errorNotification_IncorrectIdInUrl: 'Incorrect ID in URL, or record was removed',
   errorNotification_ErrorLoading: 'Error Loading',
 }

 constructor( ) { }

 track<T>(observable: Observable<T>, succeed?: SucceedFn<T>, failed?: FailedFn, finished?: FinishedFn, skipStartEvent?: boolean, notifyError: boolean = true, showErrorInDialog: boolean = false): Subscription {
   this.currentlyRunning++;

   if (!skipStartEvent) {
     this.started.emit(null);
   }
   return observable.subscribe((r) => {
     succeed && succeed(r);
   }, (e) => {
     //Emit only in case fail callback is not passed or it returns true
     if (failed) {
       failed(e);
     }

     if (e) {
     } else {
       if (notifyError) {
       }
     }

     this.tryFinish(finished);
   }, () => {
     this.tryFinish(finished)
   });
 }

 trackWithoutErrorEvent<T>(observable: Observable<T>, succeed?: SucceedFn<T>, failed?: FailedFn, finished?: FinishedFn, skipStartEvent?: boolean): Subscription {
   return this.track(observable, succeed, failed, finished, skipStartEvent, false);
 }

 trackAndNotifyError<T>(observable: Observable<T>, succeed?: SucceedFn<T>, failed?: FailedFn, finished?: FinishedFn, skipStartEvent?: boolean): Subscription {
   return this.track(observable, succeed, failed, finished, skipStartEvent, true, true);
 }

 trackWithoutStartEventAndNotifyError<T>(observable: Observable<T>, succeed?: SucceedFn<T>, failed?: FailedFn, finished?: FinishedFn): Subscription {
   return this.trackAndNotifyError(observable, succeed, failed, finished, true);
 }

 private tryFinish(finished: FinishedFn) {
   this.currentlyRunning--;
   if (this.currentlyRunning == 0) {
     //Emit only in case finished callback is not passed or it returns true
     if (!finished || finished()) {
       this.finished.emit(null);
     }
   }
 }
}